# The 0b101010 repo

This is a repository for packages used by my infrastructure. If you do not know what these packages are or what they contain, do not add the repo and do not install any packages from it. They may contain kittens, but could also contain horrible trojan horses.

So just don't use this, okay?

## I can haz a list of packages?

Well, no. But browsing the `packages` directory **and** talking to me should give you an idea.

## Arch

In `/etc/pacman.conf`

```
[kramsg1_arch_repo]
SigLevel = Optional TrustAll
Server = https://https://kramsg19981405.gitlab.io/kramsg1_repo/$arch

```

and trust the signing key

```
wget https://repo.0b101010.services/signing-key.asc
pacman-key --add signing-key.asc
pacman-key --lsign-key 208A48B8B6674E5A8F33E37580DAB1301B25A560
pacman -Sy
```
