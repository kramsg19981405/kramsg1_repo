#!/usr/bin/env sh

cd `dirname $0`

docker build -t arch-repo-builder .

docker run -t --rm \
  -v "`readlink -f ../../packages/arch`:/home/repo-builder/packages:rw" \
  -v "`readlink -f ../../public/arch`:/home/repo-builder/public:rw" \
  arch-repo-builder
